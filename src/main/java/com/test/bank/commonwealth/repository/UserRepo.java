package com.test.bank.commonwealth.repository;

import com.test.bank.commonwealth.dto.UserDto;
import com.test.bank.commonwealth.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 14/11/2020
 */
public interface UserRepo extends JpaRepository<User, Long> {

    User getById(Long id);

}
