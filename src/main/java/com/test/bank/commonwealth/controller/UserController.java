package com.test.bank.commonwealth.controller;

import com.test.bank.commonwealth.config.ApiResponse;
import com.test.bank.commonwealth.dto.CreateUserRequest;
import com.test.bank.commonwealth.dto.UpdateUserRequest;
import com.test.bank.commonwealth.entity.User;
import com.test.bank.commonwealth.repository.UserRepo;
import com.test.bank.commonwealth.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 14/11/2020
 */

@RestController
@RequestMapping("/api/user")
public class UserController {

    Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    UserRepo userRepo;

    @ApiOperation("Get All User Data")
    @GetMapping
    ResponseEntity<?> getAllUser(){

        List<User> userList = userRepo.findAll();

        return ResponseEntity.ok(new ApiResponse(true, "Success Get All Data", userList));
    }

    @ApiOperation("Get User Data By Id")
    @GetMapping("/{idUser}")
    ResponseEntity<?> getUser(@RequestParam Long id){
        User user = userRepo.getById(id);
        return ResponseEntity.ok(new ApiResponse(true, "Success Get Data", user));
    }

    @ApiOperation("Create New User")
    @PostMapping
    ResponseEntity create(@RequestBody CreateUserRequest createUserRequest) {
        User newUser = userService.create(createUserRequest);
        return ResponseEntity.ok(new ApiResponse(true, "Create Data User Success", newUser));
    }

    @ApiOperation("Update User Data")
    @PutMapping
    ResponseEntity update(@RequestBody UpdateUserRequest updateUserRequest){

        User newUser = userRepo.getById(updateUserRequest.getId());

        if (newUser == null){
            return ResponseEntity.ok(new ApiResponse(false, "Data "+updateUserRequest.getId()+" Tidak Ditemukan !"));
        }else{
            userService.update(updateUserRequest);
            return ResponseEntity.ok(new ApiResponse(true, "Update User Success", newUser));
        }
    }

    @ApiOperation("Delete User Data")
    @DeleteMapping("/{id}")
    ResponseEntity delete(@PathVariable Long id){

        User user = userRepo.getById(id);
        if (user == null){
            return ResponseEntity.ok(new ApiResponse(false, "Data "+id+" Tidak Ditemukan !"));
        }else{
            userService.delete(id);
            return ResponseEntity.ok(new ApiResponse(true, "Update User Success", user));
        }




    }

}
