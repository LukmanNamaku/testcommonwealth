package com.test.bank.commonwealth.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 14/11/2020
 */

@Data
public class CreateUserRequest {

    @NotBlank
    @NotNull(message = "error.nama-tidak-boleh-kosong")
    private String name;

    @NotBlank
    @NotNull(message = "error.phone-tidak-boleh-kosong")
    private String phone;

}
