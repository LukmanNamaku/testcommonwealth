package com.test.bank.commonwealth.dto;

import lombok.Data;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 14/11/2020
 */
@Data
public class UserDto {

    private Long id;
    private String name;
    private String phone;

}
