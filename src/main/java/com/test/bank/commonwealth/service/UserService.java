package com.test.bank.commonwealth.service;

import com.test.bank.commonwealth.config.NotFoundException;
import com.test.bank.commonwealth.dto.CreateUserRequest;
import com.test.bank.commonwealth.dto.UpdateUserRequest;
import com.test.bank.commonwealth.dto.UserDto;
import com.test.bank.commonwealth.entity.User;
import com.test.bank.commonwealth.repository.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 14/11/2020
 */

@Service
public class UserService {

    Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepo userRepo;

    public User create(CreateUserRequest createUserRequest) {
        User newUser= new User();
        BeanUtils.copyProperties(createUserRequest, newUser);
        userRepo.save(newUser);

        return newUser;
    }

    public User update(UpdateUserRequest updateRequest){

            User newUser = userRepo.getById(updateRequest.getId());

            BeanUtils.copyProperties(updateRequest, newUser);
            userRepo.save(newUser);
            return newUser;
    }

    public User delete(Long id){
        User user= userRepo.getById(id);
        userRepo.delete(user);
        return user;
    }
}
