package com.test.bank.commonwealth;

import javax.xml.transform.sax.SAXSource;
import java.util.Scanner;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 14/11/2020
 */

public class TestLogic1 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int n;

        System.out.print("Input Nilai n =");
        n = scan.nextInt();

        System.out.println("\nOutput : ");
        System.out.println("=================================");

        for (int i = 1; i <= n; i++){
            for (int j = n-1; j>= i; j--){
                System.out.print("  ");
            }
            for (int k = 1; k <= i; k++){
                System.out.print("# ");
            }
            System.out.println();
        }
    }
}
