package com.test.bank.commonwealth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommonWealthApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonWealthApplication.class, args);
    }

}
